library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package convert_so is
    function to_std_logic(bitval: bit) return std_logic;
    function hex_to_slv(hexstr: string) return std_logic_vector;
end package convert_so;


package body convert_so is

    function to_std_logic(bitval: bit) return std_logic is
    begin
        if bitval = '1' then
            return '1';
        else
            return '0';
        end if;
    end function;


    function hex_to_slv(hexstr: string) return std_logic_vector is
        variable hexval, hexdig: integer;
    begin
        hexval := 0;

        for i in 1 to hexstr'length loop
            hexdig := character'pos(hexstr(i)) - 48;
            if (hexdig >= 10) then
                hexdig := hexdig - 7;
            end if;
            assert (hexdig >= 0) and (hexdig <=15) report "Bad hex string '" & hexstr & "' passed to hex_to_slv"
                severity failure;

            hexval := (hexval * 16) + hexdig;
        end loop;

        return std_logic_vector(to_unsigned(hexval, hexstr'length*4));
    end function;

end package body convert_so;
