
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

use work.convert_so.all;

--  Define an entity without any ports for test bench
entity dgn_cart_blit_tb is
end dgn_cart_blit_tb;


architecture behaviour of dgn_cart_blit_tb is

    component dgn_cart_blit
    port (
        nrst_i  : in std_logic;
        e_i     : in std_logic;
        q_i     : in std_logic;
        nrom2_i : in std_logic;
        np2_i   : in std_logic;
        addr_io : inout std_logic_vector(15 downto 0);
        rnw_io  : inout std_logic;
        data_io : inout std_logic_vector(7 downto 0);
        nhalt_o : out std_logic;
        nnmi_o  : out std_logic;
        ncart_o : out std_logic;
        nextmem_o : out std_logic;
        snd_o   : out std_logic);
    end component;


    -- test bench signals
    signal nrst : std_logic;
    signal e, q : std_logic;
    signal nrom2, np2 : std_logic;
    signal addr : std_logic_vector(15 downto 0);
    signal rnw : std_logic;
    signal data : std_logic_vector(7 downto 0);
    signal nhalt, nnmi, ncart, nextmem, snd : std_logic;
    
    signal halt_q, halt_e : std_logic;  -- halt signal pipeline

    signal testcomplete: boolean := FALSE;
       
    -- define SAM clock period
    constant SAMCLKPERIOD: time := 1.0 sec / 14318000.0;
      
begin
    dgn_cart : dgn_cart_blit port map (
        nrst_i      => nrst,
        e_i         => e,
        q_i         => q,
        nrom2_i     => nrom2,
        np2_i       => np2,
        addr_io     => addr,
        rnw_io      => rnw,
        data_io     => data,
        nhalt_o     => nhalt,
        nnmi_o      => nnmi,
        ncart_o     => ncart,
        nextmem_o   => nextmem,
        snd_o       => snd
    );
        

    -- these signals have pullup resistors
    nhalt <= 'H';
    nnmi <= 'H';
    ncart <= 'H';
    nextmem <= 'H';

    -- initial reset
    nrst <= '0', '1' after 1 us;
   

    -- generate e & q
    process
        variable samphase: integer range 0 to 15 := 0;
    begin
        while not testcomplete loop
                        
            if samphase = 3 then
                q <= '1' after 55 ns; -- from sam datasheet
            elsif samphase = 7 then
                e <= '1' after 55 ns; -- from sam datasheet
            elsif samphase = 11 then
                q <= '0' after 25 ns; -- from sam datasheet
            elsif samphase = 15 then
                e <= '0' after 25 ns; -- from sam datasheet
            end if;
            
            wait for SAMCLKPERIOD;
            
            samphase := (samphase + 1) mod 16;
        end loop;

        wait;	-- wait forever (terminates simulation)
    end process;


    -- halt (and interrupts) sampled on falling q
    process (q)
    begin
        if falling_edge(q) then
            halt_q <= not nhalt;
        end if;
    end process;
    -- Then delay by one e clock for simulation purposes
    -- (halt & interrupts are actioned if detected prior to final cycle of current instruction)
    process (e)
    begin
        if falling_edge(e) then
            halt_e <= halt_q;
        end if;
    end process;


    -- update bus from vector file
    -- min hold times & max delays from 6809e datasheet (1MHz rating)
    process
        file infile: text is "dgn_cart_blit_tb_addr.txt";
        variable ln: line;
        variable hexstr4: string(1 to 4);
        variable hexstr2: string(1 to 2);
        variable i: natural;
        variable c: character;
    begin
        -- wait for end of initial reset
        wait until rising_edge(nrst);

        while not(endfile(infile)) loop
            readline(infile, ln);
            
            next when ln'length = 0;    -- skip empty lines
            next when ln(1) = ';';      -- skip comment lines
                
            wait until falling_edge(e);
            
            -- address and data hi-z while halted
            while halt_e = '1' loop
                addr <= (others => 'Z') after 120 ns;    -- not specified. using TSC timing
                data <= (others => 'Z') after 120 ns;    -- not specified. using TSC timing
                rnw <= 'Z' after 120 ns;    -- not specified. using TSC timing
                wait until falling_edge(e);
                if halt_e = '0' then
                    wait until falling_edge(e); -- additional dead cycle at end of halt
                end if;
            end loop;

            -- address output. indeterminate for a while.
            read(ln, hexstr4);
            addr <= (others => 'X') after 20 ns, hex_to_slv(hexstr4) after 200 ns;
            
            -- r/w indeterminate for a while
            rnw <= 'X' after 20 ns;
            
            -- old data holds for a short time
            data <= (others => 'Z') after 30 ns;

            -- check for write data
            if (ln'length >= 3) then
                rnw <= '0' after 200 ns;
                read(ln, c);    -- skip space char
                read(ln, hexstr2);
                wait until rising_edge(q);
                data <= hex_to_slv(hexstr2) after 200 ns;
            else
                rnw <= '1' after 200 ns;
            end if;

                
        end loop;

        testcomplete <= true;
            
        wait ;
    end process;
    
    
    -- simulate some memory
    process (addr, e, rnw)
        type byte_array_type is array (0 to 31) of std_logic_vector(7 downto 0);
        variable romdata : byte_array_type := (
            x"05", x"55", x"50", x"05", x"55", x"50", x"05", x"55",
            x"50", x"05", x"55", x"50", x"55", x"55", x"55", x"55",
            x"f0", x"00", x"0f", x"f0", x"00", x"0f", x"f0", x"00",
            x"0f", x"f0", x"00", x"0f", x"ff", x"ff", x"ff", x"ff");
    begin
        if (addr(15 downto 12) = x"0") and (e = '1') and (rnw = '1') then
            -- read only 'ram' at 0x0000
            data <= x"aa";
        elsif (addr(15 downto 12) = x"4") and (e = '1') and (rnw = '1') then
            -- rom at 0x4000
            data <= romdata(to_integer(unsigned(addr(4 downto 0))));
        else
            data <= (others => 'Z');
        end if;
    end process;
      

end behaviour;
