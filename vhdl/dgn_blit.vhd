-- Simple Dragon 32/64 blitter
-- S.Orchard Feb 2021
--
-- Manipulates Dragon internal RAM from cartridge port via 6809 halt & DMA
--
-- Performs shift/AND/OR operations
-- Two modes:
--   Simple source/destination copy at 2 cycles per byte
--   Image combined with masked screen background at 4 cycles per byte
-- Horizontal clip at screen edges
-- Destination size specified as width*height bytes
-- designed for 32 byte wide graphics modes only
--
--
-- DMA NOTES:
--
-- Halt should be set up before falling q (e.g. on rising e)
-- Halt is recognised on or before the second to last instruction cycle
--
-- After the current instruction has completed there is a dead cycle to allow
-- control of the bus to be transferred without contention.
-- (The cpu holds the bus for a short time after falling e)
-- It is dangerous to allow the bus to float during this time, particulary after a write cycle.
-- This cycle is usable for dma if the bus driver is enabled part way in, e.g. on rising q.
--
-- After halt is released, there are two more cycles before the cpu regains control for instruction fetch.
-- This first of these cycles is available for use.
-- The second cycle is intended to be another dead cycle for transfer of control.
-- This dead cycle could be used for operations not requiring hold time after falling e.
-- Again, it is a bad idea to let the bus float during the dead cycle.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-- define blitter interface
-- separate inputs and outputs make it easier to combine this with other modules
-- bidirectional tri-state signals for Dragon cart port should be implemented at top level
entity dgn_blit is
port (
    rst_i       : in std_logic;                         -- reset in
    e_i         : in std_logic;                         -- 6809 e clock in
    q_i         : in std_logic;                         -- 6809 q clock in
    read_i      : in std_logic;                         -- reg read enable in
    write_i     : in std_logic;                         -- reg write enable in
    addr_i      : in std_logic_vector(3 downto 0);      -- register address in
    data_i      : in std_logic_vector(7 downto 0);      -- data in
    addr_o      : out std_logic_vector(15 downto 0);    -- dma address out
    addr_o_en   : out std_logic;                        -- dma address driver enable out
    rnw_o       : out std_logic;                        -- dma r/~w out
    data_o      : out std_logic_vector(7 downto 0);     -- dma & register data out
    data_o_en   : out std_logic;                        -- data driver enable out
    halt_o      : out std_logic);                       -- halt request out
end dgn_blit;


architecture behaviour of dgn_blit is

    -- define state variable
    type dma_state_type is (
        S_READY,
        S_TRIG,
        S_READ_BG,
        S_READ_MASK,
        S_READ_IMAGE,
        S_WRITE,
        S_FINAL);

    signal dma_state        : dma_state_type;
    
    -- control registers
    signal reg_img_addr : unsigned(15 downto 0);    -- address of image data
    signal reg_msk_addr : unsigned(15 downto 0);    -- address of mask data
    signal reg_addr_v   : unsigned(10 downto 0);    -- dest vertical address / 32
    signal reg_addr_h   : unsigned(5 downto 0);     -- dest horizontal offset (6 bits allows hclip)
    signal reg_dst_wid  : unsigned(5 downto 0);     -- dest width image/mask in bytes
    signal reg_dst_hgt  : unsigned(7 downto 0);     -- dest height in lines (32*256 = 8K max)
    signal reg_rshift   : unsigned(2 downto 0);     -- image/mask right shift amount 0-7
    signal reg_maskcopy : std_logic;                -- background mask enable
    signal reg_hclip    : std_logic;                -- horizontal clip enable
    signal reg_renable  : std_logic;                -- enable right hand overflow fragment

    -- internal signals
    signal trig1, trig2     : std_logic;             -- blit op trigger signals
    signal first_cycle      : std_logic;             -- indicates cycle where 6809 releases bus
    signal addr_en          : std_logic;             -- enable signal for address output
    signal dma_write        : std_logic;             -- indicates write to memory
    signal vcount           : unsigned(7 downto 0);  -- vertical count
    signal hcount           : unsigned(5 downto 0);  -- horizontal byte count
    signal dst_addr_v       : unsigned(10 downto 0); -- vertical part of destination address
    signal dst_addr_h       : unsigned(5 downto 0);  -- horizontal part of destination address
    signal line_complete    : std_logic;             -- indicates current line is complete
    signal op_complete      : std_logic;             -- indicates whole operation is complete
    signal clip             : std_logic;             -- indicates outside of clip area
    signal shift_input_data : unsigned(15 downto 0); -- input to barrel shifter
    signal shifted_data     : unsigned(15 downto 0); -- output from barrel shifter
    signal mask_carry_data  : unsigned(7 downto 0);  -- shifted mask data carried to next byte
    signal image_carry_data : unsigned(7 downto 0);  -- shifted image data carried to next byte
    signal acc_data         : unsigned(7 downto 0);  -- general purpose accumulator
    signal output_data      : unsigned(7 downto 0);  -- data for writing back to memory

begin
    -- data output from register reads or dma writes
    data_o <=
        std_logic_vector("00" & reg_dst_wid)                    when (read_i = '1') and (addr_i = x"0") else
        std_logic_vector(reg_dst_hgt)                           when (read_i = '1') and (addr_i = x"1") else
        std_logic_vector(reg_addr_h(4 downto 0) & reg_rshift)   when (read_i = '1') and (addr_i = x"2") else
        reg_maskcopy & reg_hclip & reg_renable & "0000" & reg_addr_h(5)
                                                                when (read_i = '1') and (addr_i = x"3") else
        std_logic_vector("00000" & reg_addr_v(10 downto 8))     when (read_i = '1') and (addr_i = x"4") else
        std_logic_vector(reg_addr_v(7 downto 0))                when (read_i = '1') and (addr_i = x"5") else
        std_logic_vector(reg_msk_addr(15 downto 8))             when (read_i = '1') and (addr_i = x"6") else
        std_logic_vector(reg_msk_addr(7 downto 0))              when (read_i = '1') and (addr_i = x"7") else
        std_logic_vector(reg_img_addr(15 downto 8))             when (read_i = '1') and (addr_i = x"8") else
        std_logic_vector(reg_img_addr(7 downto 0))              when (read_i = '1') and (addr_i = x"9") else
        std_logic_vector(output_data);
        
    -- dma write is gated with clip signal
    dma_write <= '1' when (dma_state = S_WRITE) and (clip = '0') else '0';
    
    -- data output is enabled on register read or dma write
    -- reg reads gated with e as usual
    -- dma writes gated with e+q to provide time for direction change
    data_o_en <= (read_i and e_i) or (dma_write and (q_i or e_i));
    
    
    -- update registers
    process (e_i)
    begin
        if falling_edge(e_i) then
            
            -- cpu write to register
            if (write_i = '1') then
                case addr_i is
                    when x"0" =>
                        reg_dst_wid <= unsigned(data_i(5 downto 0));
                    when x"1" =>
                        reg_dst_hgt <= unsigned(data_i);
                    when x"2" =>
                        reg_addr_h(4 downto 0) <= unsigned(data_i(7 downto 3));
                        reg_rshift <= unsigned(data_i(2 downto 0));
                    when x"3" =>
                        reg_maskcopy <= data_i(7);
                        reg_hclip <= data_i(6);
                        reg_renable <= data_i(5);
                        reg_addr_h(5) <= data_i(0);
                    when x"4" =>
                        reg_addr_v(10 downto 8) <= unsigned(data_i(2 downto 0));
                    when x"5" =>
                        reg_addr_v(7 downto 0) <= unsigned(data_i);
                    when x"6" =>
                        reg_msk_addr(15 downto 8) <= unsigned(data_i);
                    when x"7" =>
                        reg_msk_addr(7 downto 0) <= unsigned(data_i);
                    when x"8" =>
                        reg_img_addr(15 downto 8) <= unsigned(data_i);
                    when x"9" =>
                        reg_img_addr(7 downto 0) <= unsigned(data_i);
                    when others =>
                        null;
                end case;
            end if;
            
            -- these registers are modified during the copy operation
            if dma_state = S_READ_MASK then
                reg_msk_addr <= reg_msk_addr + 1;
            end if;

            if dma_state = S_READ_IMAGE then
                reg_img_addr <= reg_img_addr + 1;
            end if;
            
        end if;
    end process;


    -- dma trigger signals
    trig1 <= '1' when (write_i = '1') and (addr_i = x"8") else '0';
    trig2 <= '1' when (write_i = '1') and (addr_i = x"9") else '0';

    -- signal when at end of line
    line_complete <= '1' when ((hcount = 1) and ((reg_rshift = 0) or (reg_renable = '0')))
                            or (hcount = 0) else '0';

    -- signal when whole operation is complete
    op_complete <= '1' when (vcount = 0) and (line_complete = '1') else '0';
 
    -- signal when edges should be clipped
    clip <= dst_addr_h(5) and reg_hclip;
 
 
    -- dma state machine
    process (e_i, rst_i)
    begin
        if rst_i = '1' then
            dma_state <= S_READY;
            first_cycle <= '0';
            addr_en <= '0';

        elsif falling_edge(e_i) then

            first_cycle <= '0';

            case dma_state is

                when S_READY =>
                    -- waiting for 1st trigger condition
                    if (trig1 = '1') then
                        dma_state <= S_TRIG;
                    else
                        dma_state <= S_READY;
                    end if;
    
                when S_TRIG =>
                    -- waiting for 2nd trigger condition
                    if (trig2 = '1') then
                        if reg_maskcopy = '1' then
                            dma_state <= S_READ_MASK;   -- full mask mode
                        else
                            dma_state <= S_READ_IMAGE;  -- simple copy mode
                        end if;
                        first_cycle <= '1';
                        vcount <= reg_dst_hgt;
                        hcount <= reg_dst_wid;
                        dst_addr_v <= reg_addr_v;
                        dst_addr_h <= reg_addr_h;
                        image_carry_data <= (others => '0');
                        mask_carry_data <= (others => '1');
                        acc_data <= (others => '0');
                    else
                        -- trigger didn't arrive so drop back to ready state
                        -- halt will be harmlessly asserted for one cycle
                        dma_state <= S_READY;
                    end if;

                when S_READ_MASK =>
                    dma_state <= S_READ_IMAGE;
                    addr_en <= '1';
                    shift_input_data <= unsigned(data_i) & x"ff";
                
                when S_READ_IMAGE =>
                    if reg_maskcopy = '1' then
                        dma_state <= S_READ_BG;
                        acc_data <= mask_carry_data and shifted_data(15 downto 8);
                    else
                        dma_state <= S_WRITE;
                    end if;
                    addr_en <= '1';
                    shift_input_data <= unsigned(data_i) & x"00";
                    mask_carry_data <= shifted_data(7 downto 0);
    
                when S_READ_BG =>
                    dma_state <= S_WRITE;
                    acc_data <= acc_data and unsigned(data_i);

                when S_WRITE =>
                    if op_complete = '1' then
                        -- whole operation is done
                        dma_state <= S_FINAL;
                    elsif ((hcount = 1) and (reg_rshift /= 0) and (reg_renable = '1')) then
                        -- all source bytes have been read for this line
                        -- but additional write cycle required for carried mask/image
                        if reg_maskcopy = '1' then
                            -- if full mask mode then get another background byte
                            dma_state <= S_READ_BG;
                            acc_data <= mask_carry_data;        -- no new mask data
                        else
                            -- else just need to perform another write
                            dma_state <= S_WRITE;
                        end if;
                        shift_input_data <= (others => '0');    -- no new image data
                    else
                        -- get mask or image byte depending on mode
                        if reg_maskcopy = '1' then
                            dma_state <= S_READ_MASK;
                        else
                            dma_state <= S_READ_IMAGE;
                        end if;
                    end if;

                    if line_complete = '1' then
                        -- reset for next line
                        dst_addr_h <= reg_addr_h;
                        hcount <= reg_dst_wid;
                        dst_addr_v <= dst_addr_v + 1;
                        vcount <= vcount - 1;
                        image_carry_data <= (others => '0');
                        mask_carry_data <= (others => '1');
                    else
                        dst_addr_h <= dst_addr_h + 1;
                        hcount <= hcount - 1;
                        image_carry_data <= shifted_data(7 downto 0);
                    end if;


                when S_FINAL =>
                    dma_state <= S_READY;
                    addr_en <= '0';
    
                when others =>
                    dma_state <= S_READY;
                    addr_en <= '0';
            end case;

        end if;
    end process;


    -- n bit barrel shift. fairly expensive (many muxes)
    shifted_data <= rotate_right(shift_input_data, to_integer(reg_rshift));
    
    -- final output data is masked background OR'd with image
    output_data <= acc_data or image_carry_data or shifted_data(15 downto 8);


    -- halt is updated on rising edge of e
    -- this is to meet setup time for falling q
    process (e_i, rst_i)
    begin
        if rst_i = '1' then
            halt_o <= '0';
        elsif rising_edge(e_i) then

            halt_o <= '0';

            case dma_state is
                
                when S_READY =>
                    if trig1 = '1' then
                        halt_o <= '1';
                    end if;
                    
                when S_TRIG =>
                    if trig2 = '1' then
                        halt_o <= '1';
                    end if;

                when S_READ_BG =>
                    halt_o <= '1';
                    
                when S_READ_MASK =>
                    halt_o <= '1';
                    
                when S_READ_IMAGE =>
                    halt_o <= '1';
                    
                when S_WRITE =>
                    if op_complete = '0' then
                        halt_o <= '1';
                    end if;
                    
                when others =>
                    halt_o <= '0';
                    
            end case;
        end if;
    end process;


    -- address output
    addr_o <=   std_logic_vector(dst_addr_v & dst_addr_h(4 downto 0)) when (dma_state = S_READ_BG) else
                std_logic_vector(reg_msk_addr) when (dma_state = S_READ_MASK) else
                std_logic_vector(reg_img_addr) when (dma_state = S_READ_IMAGE) else
                std_logic_vector(dst_addr_v & dst_addr_h(4 downto 0)) when (dma_write = '1') else
                x"FFFF";
    
    -- read/write output
    rnw_o <= not dma_write;

    -- address enable output
    -- enable held off for 1st quarter of first cycle to avoid contention with cpu
    addr_o_en <= (first_cycle and (q_i or e_i)) or addr_en;
       

end behaviour;
