-- Simple Dragon 32/64 blitter
-- S.Orchard Feb 2021
--
-- Manipulates Dragon internal RAM from cartridge port via 6809 halt & DMA
--
-- Tested on a real Dragon with Spartan3 FPGA. 5V/3V3 level conversion via resistors.
--   1K: address, r/w, data, (unused r2, p2)
--   220R: e, q, halt (unused nmi, cart, extmem, snd)
--
-- e & q connected via GPIO, not dedicated clock inputs.
-- (e & q are not clean enough to drive clock nets directly)
--
--
-- Dragon 64s and MKII Dragon 32s do not reset reliably with above arrangement.
-- The reset capacitor charges via the halt line pullup resistor on these machines.
-- The 3V3 i/o clamp diodes lower the halt line voltage.
-- This prevents the reset cap charging above the 4V reset release threshold.
-- It would be better to drive the halt line with a fast transistor.
--
--
-- Another issue is with the SYNC instruction.
-- This allows the Dragon bus to float which isn't normally an issue
-- with a stock machine, but the external circuitry seems to encourage the
-- bus to drop and write to random locations in memory.
-- One solution is to avoid the sync instruction.
-- Another is to fit pullup resistors to the address bus (like the CoCo)
-- This makes interfacing to 3V3 logic via series resistors less feasible.
-- (Lower value resistors / hotter 6809 / bigger danger of pushing up 3V3 rail)


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Dragon cartridge port signals
entity dgn_cart_blit is
port (
    nrst_i  : in std_logic;                         -- reset
    e_i     : in std_logic;                         -- e clock
    q_i     : in std_logic;                         -- q clock
    nrom2_i : in std_logic;                         -- cartridge rom select (0xc000 - 0xfeff)
    np2_i   : in std_logic;                         -- secondary device select (0xff40 - 0xff5f)
    addr_io : inout std_logic_vector(15 downto 0);  -- address bus
    rnw_io  : inout std_logic;                      -- r/~w
    data_io : inout std_logic_vector(7 downto 0);   -- data bus
    nhalt_o : out std_logic;                        -- halt request
    nnmi_o  : out std_logic;                        -- nmi request
    ncart_o : out std_logic;                        -- cart interrupt request
    nextmem_o : out std_logic;                      -- external memory select
    snd_o   : out std_logic                         -- external sound
   );
end dgn_cart_blit;


architecture behaviour of dgn_cart_blit is

    -- internal active high signals
    signal reset    : std_logic;
    signal halt     : std_logic;
    signal nmi      : std_logic;
    signal cart     : std_logic;
    signal extmem   : std_logic;


    -- declare blitter component interface
    component dgn_blit
    port (
        rst_i       : in std_logic;                         -- reset in
        e_i         : in std_logic;                         -- 6809 e clock in
        q_i         : in std_logic;                         -- 6809 q clock in
        read_i      : in std_logic;                         -- reg read in
        write_i     : in std_logic;                         -- reg write in
        addr_i      : in std_logic_vector(3 downto 0);      -- reg address in
        data_i      : in std_logic_vector(7 downto 0);      -- data in
        addr_o      : out std_logic_vector(15 downto 0);    -- dma address out
        addr_o_en   : out std_logic;                        -- address driver enable out
        rnw_o       : out std_logic;                        -- dma r/~w out
        data_o      : out std_logic_vector(7 downto 0);     -- dma & register data out
        data_o_en   : out std_logic;                        -- data driver enable out
        halt_o      : out std_logic);                       -- halt request out
    end component;

    -- blitter component signals
    signal blt_read_i    : std_logic;
    signal blt_write_i   : std_logic;
    signal blt_data_o    : std_logic_vector(7 downto 0);
    signal blt_data_o_en : std_logic;
    signal blt_addr_o    : std_logic_vector(15 downto 0);
    signal blt_addr_o_en : std_logic;
    signal blt_rnw_o     : std_logic;
    signal blt_halt_o    : std_logic;

begin

    -- create active high reset signal
    reset <= not nrst_i;

    -- blitter register read and write signals (ff70-ff7f)
    blt_read_i <= '1' when (addr_io(15 downto 4) = x"FF7") and (rnw_io = '1') else '0';
    blt_write_i <= '1' when (addr_io(15 downto 4) = x"FF7") and (rnw_io = '0') else '0';

    -- output data for selected device or remain high impedance
    data_io <=
        blt_data_o when (blt_data_o_en = '1') else
        (others => 'Z');


    -- create blitter instance
    dgn_blit1 : dgn_blit port map (
        rst_i       => reset,
        e_i         => e_i,
        q_i         => q_i,
        read_i      => blt_read_i,
        write_i     => blt_write_i,
        addr_i      => addr_io(3 downto 0),
        data_i      => data_io,
        addr_o      => blt_addr_o,
        addr_o_en   => blt_addr_o_en,
        rnw_o       => blt_rnw_o,
        data_o      => blt_data_o,
        data_o_en   => blt_data_o_en,
        halt_o      => blt_halt_o
    );


    -- blitter drives halt line and address bus
    halt    <= blt_halt_o;
    addr_io <= blt_addr_o when (blt_addr_o_en = '1') else (others => 'Z');
    rnw_io  <= blt_rnw_o when (blt_addr_o_en = '1') else 'Z';


    -- unused signals
    nmi     <= '0';
    cart    <= '0';
    extmem  <= '0';


    -- these signals have pullup resistors and require open drain style of drive
    nhalt_o     <= '0' when (halt = '1') else 'Z';
    nnmi_o      <= '0' when (nmi = '1') else 'Z';
    ncart_o     <= '0' when (cart = '1') else 'Z';
    nextmem_o   <= '0' when (extmem = '1') else 'Z';

    -- sound output not used
    snd_o <= 'Z';


end behaviour;
