## Dragon Cartridge Port Blitter

VHDL code implementing a simple blitter with an interface for Dragon and CoCo 1/2 machines.

Not really intended to be a finished product, this is currently just a proof of concept but has been successfully tested on real hardware using a Spartan3 FPGA board.

### Blitter Features

* Halts the processor and uses DMA (direct memory access) to read graphics data from Dragon memory, perform an n-bit right shift, optionally apply an AND/OR operation with the background, and then write the result back to Dragon memory.
* Linear source data is copied to a rectangular region.
* Copy operations take 2 cycles per byte transferred.
* Full mask operations take 4 cycles per final image byte.
* The horizontal address offset is signed allowing simple clipping at the left and right edges of the screen. 
* Designed for 32 bytes per line video modes only.

### Motivation

This project was brought about mainly to satisfy my curiosity on the following:

* Interfacing programmable logic via the Dragon cartridge port.
* Feasibility of interfacing to 3V3 logic via series resistors.
* Develop a working model for DMA transfers.
* I've always wanted to see a graphics accelerator in action on the Dragon.

### Included Files

The 'vhdl' folder contains the design files and a testbench. The files contain more detailed notes on how the DMA works plus register descriptions etc. Also included is a Xilinx ise constraints file (ucf) for the XESS XSA-3S1000 FPGA board, just in case someone else has one on these.

The 'asm' folder contains an assembly language test program 'blit.s' demonstrating how to drive the blitter hardware.

### Implementation Notes

As already mentioned, I used a Spartan3 FPGA board for experimentation and testing. The connection to the Dragon cartridge port was achieved with an [extension pcb](http://store.go4retro.com/tandy/tandy-color-computer-coco-dragon-cartridge-extension-pcb/) from the Retro Innovations online store.

As the Dragon has 5V logic, and the FPGA board has 3V3 logic, some level conversion was necessary. Surprisingly, series resistors were good enough for experimentation at least. I find it useful to solder resistors to double row pin headers. This makes it easy to hook everything together with Dupont style jumper wires.

* 1K resistors: address, r/w, data, (unused r2, p2)
* 220 Ohm resistors: e, q, halt (unused nmi, cart, extmem, snd)

I've chosen the resistors to limit the clamp current to well within the datasheet specification of the FPGA. The lower values for e and q were required to maintain a decent clock waveform. The other low values were necessary to adequately pull down the signals in the Dragon that have pull up resistors.

One thing to be careful of is to be sure that the clamp current does not push up the 3V3 supply, as it may damage the 3V3 logic. This will happen if the current flowing into the 3V3 rail from the I/O clamps exceeds the current consumption of the circuit. This is probably not an issue with a large power-hungry FPGA, but is a very real risk with a low power CPLD. One cheating way is to deliberately increase the current consumption by connecting a resistor between 3V3 and 0V.

I selected low slew rates for the fpga outputs to reduce overshoot and ringing. The series resistors also help with this.

To help fit the design into the smallest possible device it is advisable to select area or density as a design goal rather than speed.

I found that the E and Q clocks from the cartridge port are not really clean enough to drive a clock net directly and could only get reliable results by connecting them to GPIO pins. The timing difference is insignificant at 6809 speeds.

### Issues

The reset line is a bit of a problem. On some machine types this has a capacitor charged from a 100K pullup resistor, and on others the reset capacitor is charged via the 4K7 resistor on the HALT line. Any clamp current flowing would lower the voltage on the reset line and possibly prevent the 6809 resetting. A simple solution would be to insert a series Schottky diode pointing to the Dragon and enable the pullup resistor in the FPGA. I didn't bother as the HALT line problem is worse.

The design needs to drive the HALT line to perform DMA transfers. As this line has a 4K7 pullup, a relatively low series resistance is required to drive it. On some Dragon variants, this will prevent the reset capacitor charging above 4V, which is the level at which the 6809 is specified to come out of reset. A simple solution would be to drive the HALT line from something like an open drain mosfet. I didn't bother and just put up with the flaky reset.

Another issue concerns the SYNC instruction. This causes the 6809 bus to become high impedance and float until an interrupt is detected. This does not appear to cause a problem with stock Dragons as the TTL era logic seems to be able to hold itself at a high level for some time. However, with modern CMOS logic attached, this seems to encourage the signals to drop to a low level causing spurious writes to memory and general chaos.

One solution is to avoid the SYNC instruction. Another is to fit pullup resistors to the address and r/w lines. (Tandy CoCo machines already have these resistors.) The caveat is that lower value series resistors would be required to be able to pull the signals sufficiently low. This comes with an increased risk of pushing up the 3V3 rail.

Of course many of these issues can be avoided by using programmable logic with 5V tolerant I/O, or by using active level conversion such as 74LVT series buffers.

### Possible Improvements

Many improvements could be made to add features and improve performance. These are some ideas:

* Clipping horizontally and vertically against arbitrary rectangles.
* Iterate over the unclipped area only.
* Store graphics data in local SRAM giving a large increase in performance.
* Character map / tile graphics accelerator

### Acknowledgements

Jim Brain's excellent [series of articles](https://www.go4retro.com/2020/02/28/coco-dma-early-efforts/) describing his investigation into DMA on the Tandy CoCo make for an interesting read. I would recommend reading these if you're interested in developing your own DMA hardware.
