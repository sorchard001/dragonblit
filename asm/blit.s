; Dragon 32/64 cartridge port blitter test program
; S. Orchard Feb 2021
;
; Assemble with asm6809
;


; define number of sprites in demo
NUMSPRITES      equ 14


; set SW_BLIT to 1 for software simulation (very slow!)
SW_BLIT equ 0


 if SW_BLIT
BLIT_BASE equ sw_blit_regs
 else
BLIT_BASE equ $ff70
 endif

; blit registers
BLIT_WID equ BLIT_BASE+0        ; destination width 1-63 bytes
BLIT_HGT equ BLIT_BASE+1        ; destination height 1-255 bytes
BLIT_HAD equ BLIT_BASE+2        ; horizontal address + right shift value
BLIT_CTL equ BLIT_BASE+3        ; control bits: b7 mask mode b6 hclip b5 rfrag b0 haddr msb
BLIT_VAD equ BLIT_BASE+4        ; vertical address >> 5
BLIT_MSK equ BLIT_BASE+6        ; mask address
BLIT_IMG equ BLIT_BASE+8        ; image address & trigger

; blit control reg bits
BLIT_CTRL_MASK      equ $80     ; enable mask mode
BLIT_CTRL_HCLIP     equ $40     ; enable horizontal clipping
BLIT_CTRL_RFRAG     equ $20     ; enable write of carried data at right hand edge
BLIT_CTRL_HMSB      equ $01     ; horizontal address MSB (signed)

; addresses of video buffers
; if these are changed then the SAM writes below also need to be changed
VIDBASE0        equ $600
VIDBASE1        equ VIDBASE0+$1800
; video buffer addresses scaled for blitter
YBASE0          equ VIDBASE0 >> 5
YBASE1          equ VIDBASE1 >> 5

; macros to switch SAM between video buffers
; these should be kept consistent with the video buffer addresses
mac_sam_vid_init macro
        sta $ffc7       ; +0200
        sta $ffc9       ; +0400
        endm
mac_sam_vid0 macro
        sta $ffca       ; -0800
        sta $ffcc       ; -1000
        endm
mac_sam_vid1 macro
        sta $ffcb       ; +0800
        sta $ffcd       ; +1000
        endm


; start address of background graphics
BG_SOURCE       equ $6000

; ---------------------------------------------------------
; Program entry

        org $5000
        put *
        
        orcc #$50
        
        jsr prepare_bg
        jsr init_sprites

        ; graphics mode mono 6K
        lda #$f0
        sta $ff22
        sta $ffc0
        sta $ffc3
        sta $ffc5

        mac_sam_vid_init
        clr frameflag

loop        
        jsr flip_buffers        ; swap front & back video buffers

        lda $ff02               ; clear vsync irq flag
1       lda $ff03               ; wait for vsync
        bpl 1b                  ;  (avoiding SYNC instruction)
       
        bsr draw_bg
        bsr draw_sprites

        ldd #$fb40      ; check for break key
        sta $ff02       ;
        bitb $ff00      ;
        bne loop        ;
        
        rts

; ---------------------------------------------------------
; draw and animate all sprites

draw_sprites
        ldx #sprites
        ldd #$020f
        std BLIT_WID

sp_loop
        lda SP_XPOS,x
        ldb #BLIT_CTRL_MASK + BLIT_CTRL_RFRAG
        std BLIT_HAD

        clra
        ldb SP_YPOS,x
        addd ybase
        std BLIT_VAD

        ldd #sprite_mask_data
        std BLIT_MSK

        ldd #sprite_image_data
        std BLIT_IMG

    if SW_BLIT
        pshs x
        jsr sw_blit
        puls x
    endif

        ; update sprite position
        
        ldd SP_XPOS,x
        addd SP_XVEL,x
        std SP_XPOS,x
        
        cmpa #1
        bls 1f
        cmpa #254-16
        blo 2f
1
        ldd SP_XVEL,x
        coma
        comb
        addd #1
        std SP_XVEL,x
2
        ldd SP_YVEL,x
        addd #32        ; gravity effect
        std SP_YVEL,x
        addd SP_YPOS,x
        std SP_YPOS,x

        cmpa #1
        bls 5f
        cmpa #191-16
        blo 9f
5       
        ldd SP_YVEL,x
        coma
        comb
        addd #1
        std SP_YVEL,x
        addd SP_YPOS,x
        std SP_YPOS,x
9       
        leax SP_SIZE,x
        cmpx #end_sprites
        blo sp_loop

        rts

; ---------------------------------------------------------
; draw and animate background

draw_bg
        ldd #$22bf
        std BLIT_WID
        
bg_shift equ *+1        
        lda #-8
        ldb #BLIT_CTRL_HCLIP + BLIT_CTRL_HMSB
        std BLIT_HAD

        ldd ybase
        std BLIT_VAD

bg_hoff equ *+2
        ldx #BG_SOURCE+1
bg_voff equ *+1
        lda #0
        anda #15
        ldb #34
        mul
        leax d,x
        stx BLIT_IMG

bg_ypos equ *+1
        ldd #0
bg_yvel equ *+1
        addd #0
        std bg_ypos
        sta bg_voff
        ldd bg_yvel
bg_yacc equ *+1
        addd #5
        std bg_yvel
        
        cmpd #$200
        bge 1f
        cmpd #-$200
        bgt 2f
        
1       ldd bg_yacc
        comb
        coma
        addd #1
        std bg_yacc
2
        lda bg_shift
        deca
        anda #7
        cmpa #7
        bne 1f
        ldb bg_hoff
        eorb #1
        stb bg_hoff

1       ora #$f8        ; horizontal address fixed at -1
        sta bg_shift
        
    if SW_BLIT
        jmp sw_blit
    else
        rts
    endif

; ---------------------------------------------------------
; prepare background graphics

prepare_bg
        ldy #BG_SOURCE
        leas -3,s
        lda #17         ; number of columns of tiles
        sta 2,s

3       leax ,y
        lda #13         ; number of rows of tiles
        sta 1,s
2       ldu #bg_tile_data
        lda #16         ; lines per tile
        sta ,s
1       ldd ,u++
        std ,x
        leax 34,x       ; buffer width
        dec ,s
        bne 1b
        dec 1,s
        bne 2b
        leay 2,y
        dec 2,s
        bne 3b
        
        puls a,x,pc

; ---------------------------------------------------------
; initialise sprites with random positions and velocities

init_sprites
        ldx #sprites
        clrb
       
1       bsr rnd_number
        lsra
        adda #64
        std SP_XPOS,x

        bsr rnd_number
        lsra
        adda #32
        std SP_YPOS,x
        
        bsr rnd_number
        tfr a,b
        sex
        lslb
        rola
        lslb
        rola
        std SP_XVEL,x
        
        bsr rnd_number
        tfr a,b
        sex
        std SP_YVEL,x

        leax SP_SIZE,x
        cmpx #end_sprites
        blo 1b
        
        rts
        
; ---------------------------------------------------------
; xabc pseudo-random number generator
; returns random number in A
; converted from c source found on www.eternityforest.com
	
rnd_number
rndx    lda #0
        inca
        sta rndx+1
rnda    eora #0
rndc    eora #0
        sta rnda+1
rndb    adda #0
        sta rndb+1
        lsra
        adda rndc+1
        eora rnda+1
        sta rndc+1
        rts

; ---------------------------------------------------------
; swap front and back video buffers

flip_buffers
        com frameflag
        beq 1f
        ldd #VIDBASE1
        std backbuf
        ldd #YBASE1
        std ybase
        mac_sam_vid0
        rts
1       ldd #VIDBASE0
        std backbuf
        ldd #YBASE0
        std ybase
        mac_sam_vid1
        rts

; ---------------------------------------------------------
; (Slow) simulation of blitter hardware
;
; Uses self-modifying & conditional code.
; If you came here looking for elegance
; then prepare to be disappointed...
;

 if SW_BLIT

mac_sw_blit macro

        lda BLIT_HAD    ; amount of right shift
        anda #7         ;
        bne 50f         ; non-zero shift

; zero shift        
        stb 10f+1       ; horizontal offset
 if \1  ; mask mode
        ldy BLIT_MSK    ; point to mask data
 endif

2       lda BLIT_WID    ; horizontal count
        sta hcnt        ;
10      ldb #0          ; horizontal offset

 if \1  ; mask mode
1       lda ,y+         ; get mask byte
        anda b,x        ; AND with background
        ora ,u+         ; or with image byte
 else
1       lda ,u+         ; get image byte
 endif
 if \2  ; h clip
        bitb #32        ; horizontal clip check
        bne 3f          ;
 endif
        sta b,x         ; store to background
3       incb
        dec hcnt
        bne 1b
        
        leax 32,x       ; next row
        dec vcnt
        bne 2b
        
        rts

; non-zero shift
50      stb 10f+1       ; horizontal offset
        ldy #shift_table
        lsla
        ldd a,y
        sta 20f+1       ; image degree of shift
 if \1  ; mask mode
        sta 21f+1       ; mask degree of shift
        stb 40f+1       ; left mask fragment
        comb
        stb 41f+1       ; right mask fragment
        ldy BLIT_MSK
 endif
        ldb #$20        ; BRA opcode
        lda BLIT_CTL
        bita #BLIT_CTRL_RFRAG
        beq 1f
        incb            ; BRN opcode
1       stb 60f        

2       lda BLIT_WID
        sta hcnt
        clr 30f+1       ; image carry
10      ldb #0          ; horizontal offset
        stb 11f+2
 if \1  ; mask mode
        stb 12f+2
40      lda #0          ; left mask fragment
        sta 31f+1       ; mask carry

1       lda ,y+         ; get mask byte
21      ldb #0          ; degree of shift
        mul
31      ora #0          ; mask carry
        stb 31b+1
12      anda <0,x       ; AND with background
        sta 5f+1        ; save for later
        inc 12b+2       ; h offset
 else
1
 endif
        lda ,u+         ; get image byte
20      ldb #0          ; degree of shift
        mul
30      ora #0          ; image carry
        stb 30b+1
 if \2  ; h clip
        ldb 11f+2       ; h offset
        bitb #32        ; h clip check
        bne 3f
 endif
 if \1  ; mask mode
5       ora #0          ; OR with masked background byte
 endif
11      sta <0,x        ; store to background
3       inc 11b+2       ; h offset
        dec hcnt
        bne 1b

        ; carried fragment at end
60      brn 3f
        ldb 11b+2       ; h offset
 if \2  ; h clip
        bitb #32        ; h clip check
        bne 3f
 endif
 if \1  ; mask mode
        lda 31b+1       ; mask carry
41      ora #0          ; right mask fragment
        anda b,x
        ora 30b+1       ; image carry
 else
        lda 30b+1       ; image carry
 endif
        sta b,x

3       leax 32,x
        dec vcnt
        bne 2b
        
        rts
        
        endm


sw_blit
        ldu BLIT_IMG    ; point to image data
        
        ldd BLIT_VAD    ; vertical part of address
        lslb            ; multiply by 32
        rola            ;
        lslb            ;
        rola            ;
        lslb            ;
        rola            ;
        lslb            ;
        rola            ;
        lslb            ;
        rola            ;
        tfr d,x         ;

        lda BLIT_HGT    ; vertical count
        inca            ;
        sta vcnt        ;
        
        lda BLIT_CTL    ; horizontal address offset
        ldb BLIT_HAD    ;
        lsra            ;  sign bit in BLIT_CTL
        rorb            ;  plus upper 5 bits of BLIT_HAD
        asrb            ;
        asrb            ;

        lda BLIT_CTL    ; determine which routine is required
        lsla
        rola
        rola
        anda #3
        lsla
        ldy #sw_blit_table
        jmp [a,y]


sw_blit_table   fdb sw_blit_nomask_nohclip
                fdb sw_blit_nomask_hclip
                fdb sw_blit_mask_nohclip
                fdb sw_blit_mask_hclip
        
sw_blit_nomask_nohclip
        mac_sw_blit 0,0

sw_blit_nomask_hclip
        mac_sw_blit 0,1

sw_blit_mask_nohclip
        mac_sw_blit 1,0

sw_blit_mask_hclip
        mac_sw_blit 1,1

; table of multipliers and left masks
shift_table     fdb 0,$8080,$40c0,$20e0,$10f0,$08f8,$04fc,$02fe

hcnt            rmb 1
vcnt            rmb 1
sw_blit_regs    rmb 10

 endif ;SW_BLIT
 
; ---------------------------------------------------------
        
sprite_mask_data
        include "spritem.s"

sprite_image_data
        include "sprite.s"

bg_tile_data
        include "tile.s"
        
; ---------------------------------------------------------

frameflag       rmb 1
backbuf         rmb 2
ybase           rmb 2



sprites         rmb SP_SIZE * NUMSPRITES
end_sprites     equ *

        org 0

SP_XPOS         rmb 2
SP_YPOS         rmb 2
SP_XVEL         rmb 2
SP_YVEL         rmb 2
SP_SIZE         equ *
